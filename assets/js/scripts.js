$(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});


smoothScroll.init();
$(document).ready(function() {
  $('.menu-link').menuFullpage();
});

// copyright date in footer
// get current year for copyright footer
$(".copy-year").text( (new Date).getFullYear() );


// equal height timeline
$('.card-timeline').equalHeight();


$(function() {
  $('.navbar-toggler').on('click', function(e) {
    $('.navbar').toggleClass('open');
  });
});




$(window).scroll(function(){
   $('nav').toggleClass('scrolled', $(this).scrollTop() > 150);
   if ($(this).scrollTop() > 150) {
     $('.navbar-brand img').attr('src','https://www.synogix.com/assets/img/svg/logo-footer.svg');
     $('.navbar-dark .navbar-toggler').css('color', 'rgb(31,98,175,1)').css('border-color', 'rgb(31,98,175,1)');
   }
   if ($(this).scrollTop() < 150) {
     $('.navbar-brand img').attr('src','https://www.synogix.com/assets/img/svg/mobile-logo.svg');
     $('.navbar-dark .navbar-toggler').css('color', 'rgb(255,255,255,1)').css('border-color', 'rgb(255,255,255,1)');
   }
 });

 $(window).on('scroll', function () {
     var scrollTop = $(window).scrollTop();
     if (scrollTop < 1) {
         $('.navbar').css("top", "27px");
     }
     else {
          $('.navbar').css("top", "0px");
     }
 });
