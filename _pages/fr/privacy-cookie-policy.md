---
layout: pages
title: Privacy and cookie policy
ref: privacy-cookie-policy
description: Synogix est votre fournisseur de services logistiques, avec des solutions pour la logistique avancée et inversée, des logiciels de logistique et l'exécution omnicanale. Nous offrons une gestion logistique rentable pour les marques.
keywords: service logistique, prestataire de services logistiques, solutions logistiques, solutions 3pl, gestion logistique, logistique de la mode, distribution omnicanale, exécution omnicanale, solutions de chaîne d'approvisionnement, logistique de distribution
lang: fr
---

##	1. Welcome to our website

We attach great importance to your privacy and therefore want to be transparent about the data we collect about you and the way we process your data. Please read this carefully. This privacy policy is effective when you visit our website and when you use our services.

The personal data obtained through this website will be included in the database of Synogix/FLA Europe NV, Lindestraat 58, 9700 Oudenaarde, Belgium.

---

## 2. Which information do we collect?

### 2.1 Personal data

We collect personal data when filling in the contact form. More specifically, we collect the following contact information:

- First name
- Last name
- Email address
- Mobile phone number
- Company name

We need to collect this information to get in touch with you. It is our commitment to only use this information internally to:

- Contact you regarding your appointment or question.
- Send information, newsletters and promotions.

For the implementation of this contact form and the collection of your data, we use HubSpot. The lifespan of the data is also determined by HubSpot. [Here you can find HubSpot’s privacy policy](https://legal.hubspot.com/privacy-policy).

### 2.2 Newsletters and promotions

When subscribing to our newsletter, we collect your name and email address. We will only use this data to send you information, newsletters and promotions.

You will always be able to unsubscribe if you no longer want to receive emails from us.

For newsletters and promotions, we also work with HubSpot. [Here you can find HubSpot’s privacy policy](https://legal.hubspot.com/privacy-policy).

When you open our newsletter, HubSpot collects a few statistics to analyze its performance. These include information about your browser type, unique identifier, and the number of times you open the newsletter and/or click on a link. The lifespan of these data is determined by HubSpot.

### 2.3 Payment information

Our website does not collect credit card or payment information.

### 2.4 Client information

If you are a customer, we also store your contact and billing information. This personal data will be kept for as long as necessary to let our collaboration run smoothly and to give you access to our secured client portal.

We keep data for an indefinite period of time. There is a legal obligation for the retention period for data, such as the tax obligation of seven years for payment data.

### 2.5 Cookies

##### What are cookies?

Cookies are small pieces of data sent from a website and stored on the user's computer by the user's web browser while the user is browsing.

##### Why do we use cookies?

By using cookies, we can offer you the best user experience. Thanks to cookies we can:

- Constantly optimize our website;
- Remember your preferences so that you don't have to enter or download the same information over and over again when you use our website;
- Improve the security and speed of our website;
- Make our marketing more efficient so that we can provide you with a better service.

##### Can I refuse the use of cookies?

Most browsers allow you to accept or block cookies and inform you of the use of cookies. You can choose to decline these cookies. However, this can interfere with the navigation and functionality of the website. Do you still want to refuse the use of cookies? Then you can find the instructions for different browsers below:

- [Mozilla Firefox](https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox)
- [Internet Explorer](https://support.microsoft.com/en-us/windows/delete-and-manage-cookies-168dab11-0753-043d-7c16-ede5947fc64d)
- [Google Chrome](https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DDesktop&hl=en)
- [Safari (iOS)](https://support.apple.com/en-euro/HT201265)
- [Safari (macOS)](https://support.apple.com/en-euro/guide/safari/sfri11471/mac#:~:text=Remove%20stored%20cookies%20and%20data,click%20Remove%20or%20Remove%20All.)
- [Edge](https://support.microsoft.com/en-us/microsoft-edge/delete-cookies-in-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09)

##### What cookies do we use?

###### Necessary cookies

Necessary cookies are first-party cookies that only collect information on the website itself. They are needed to guarantee the functionality of our site.

How long are these stored?

- Web session: 1 session
- Cookie statement: 1 month
- Session security cookie: 1 year

###### Analytical cookies

Analytical cookies are third-party cookies that collect information about the behavior of website visitors and the performance of the website. This information includes browser type, IP address, operating system and the domain name of the website that sent you to our website.

We collect this information anonymously to improve our website and to offer visitors a better user experience. To do this, we use Google Analytics and GA audiences. Lifespan determined by google.

More information about the data processing of Google Analytics can be found through this link: [google privacy & terms](https://policies.google.com/privacy?hl=en).

Our site also implements Google anonymization for IP addresses. More info: [IP anonymization in Analytics](https://support.google.com/analytics/answer/2763052?hl=en)

You can also choose to disconnect Google Analytics. [Google Analytics Opt-out Browser Add-on](https://tools.google.com/dlpage/gaoptout?hl=en)

###### Marketing cookies

Marketing cookies are third-party cookies that track the behavior of website visitors and create a user profile based on this information. This way, on other websites you get more personalized advertisements based on your interests.

The lifespan of these cookies is determined by their linked tool. Third parties that possibly place cookies:

- [Google Adwords and Google remarketing](https://policies.google.com/technologies/cookies?hl=nl&gl=be)
- [Hubspot](https://legal.hubspot.com/privacy-policy)

You can manage your preferences through [www.youronlinechoices.com](https://www.youronlinechoices.com/). This will not result in not getting any more adds while surfing. The adds will only be more generic and not based on your personal preferences.

---

## 3.	Third-party access

Your information will only be processed internally and will not be passed on to third parties.

---

## 4.	Security and Privacy

We have developed security measures that are technically and organizationally adapted to avoid the destruction, loss, forgery, modification, unauthorized access or notification by mistake to third parties of personal data collected on the website, as well as any other unauthorized processing of these data.

---

## 5.	Links to other sites

This website includes links to sites. Synogix cannot guarantee that these websites comply with the privacy policy of the General Data Protection Regulation (GDPR), in the Dutch General Provision Data Protection (AVG) No. 2016/679 of May 24, 2016. We therefore advise users to verify this by consulting the privacy clauses that must appear on every website.

---

## 6.	Data protection rights

As a website visitor, you have several rights relating to your personal data.

### 6.1 Right to access and adjust

At any moment you have the right to check and change your personal data, free of charge. This can be done by sending an email to privacy_request@synogix.com with a copy of your ID.

### 6.2 Right of objection

You can exercise your right of objection to the processing of your personal data for serious and legitimate reasons by sending an email to [privacy_request@synogix.com](mailto:privacy_request@synogix.com) with a copy of your ID. However, you cannot oppose data if this is necessary for the performance of contractual obligations.

### 6.3 Right to be forgotten

You always have the right to have your data removed from our databases.

---

## 7.	Contact

For any additional information or comments about this privacy policy or regarding the way we collect data, please contact Synogix at the following address:

Synogix/FLA Europe<br>
Lindestraat 58<br>
9700 Oudenaarde<br>
Belgium<br>
Attn. Privacy Officer<br>
BTW: BE0420.212.809<br>
[privacy_request@synogix.com](mailto:privacy_request@synogix.com)

Your complaint or dispute must be sent to the above address. All communications from Synogix to you will go through email.

---

## 8.	Modifiability

Synogix has the right to change this privacy statement if necessary. Therefore, it is important to check this page regularly. The privacy policy was last modified on December 9, 2021.
